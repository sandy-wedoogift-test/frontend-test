# FrontendTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.

To run the project, first use `npm install` command, then `npm start`
Note that this project require the [calculator-server](https://gitlab.com/wedoogift1/frontend-test/-/tree/main/calculator-server) project for any server communication.