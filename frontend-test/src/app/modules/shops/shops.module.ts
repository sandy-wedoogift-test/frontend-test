import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShopsRoutingModule } from './shops-routing.module';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { ShopListComponent } from './pages/shop-list/shop-list.component';
import { SearchCombinationComponent } from './pages/search-combination/search-combination.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    LandingPageComponent,
    ShopListComponent,
    SearchCombinationComponent,
    CalculatorComponent
  ],
  imports: [
    CommonModule,
    ShopsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ShopsModule { }
