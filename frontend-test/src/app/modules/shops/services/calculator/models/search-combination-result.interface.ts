export interface SearchCombinationResult {
    "equal": CalculatorComponentValue;
    "floor": CalculatorComponentValue;
    "ceil": CalculatorComponentValue;
}

export interface CalculatorComponentValue {
    "value": number;
    "cards": Array<number>;
}