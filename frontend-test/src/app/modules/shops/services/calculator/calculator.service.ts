import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchCombinationResult } from './models/search-combination-result.interface';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  constructor(private httpClient: HttpClient) { }

  searchCombination(amount: number, shopId: number): Observable<SearchCombinationResult> {
    const params = {
      amount: amount
    };

    const headers = {
      'Authorization': 'tokenTest123'
    }

    return this.httpClient.get<SearchCombinationResult>(`/shop/${shopId}/search-combination`, {params: params, headers: headers});
  }
}
