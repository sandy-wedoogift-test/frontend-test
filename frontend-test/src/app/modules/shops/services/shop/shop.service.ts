import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GetShopListResult } from './models/get-shop-list-result.interface';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  constructor() { }

  getShopList(): Observable<GetShopListResult> {
    //données en dure pour le moment
    return of({
      shopList: [
        {id: 5, imgId: 'wedoogift', name: 'WedooStore'}
      ]
    })
  }
}
