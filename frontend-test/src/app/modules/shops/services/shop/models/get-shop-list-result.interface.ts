import { Shop } from "../../../models/shop.interface";

export interface GetShopListResult {
    shopList: Array<Shop>;
}