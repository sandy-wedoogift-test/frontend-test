import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ControlContainer, ControlValueAccessor, FormControl, FormControlDirective, FormGroup, NG_VALUE_ACCESSOR, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CalculatorService } from '../../services/calculator/calculator.service';
import { SearchCombinationResult, CalculatorComponentValue } from '../../services/calculator/models/search-combination-result.interface';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: CalculatorComponent
    }
  ]
})
export class CalculatorComponent implements OnInit, AfterViewInit, ControlValueAccessor {
  private subscription: Subscription;

  @Input() shopId!: number;
  @Output() onChangeAmount: EventEmitter<number> = new EventEmitter();

  form: FormGroup;
  searchCombinationResult!: SearchCombinationResult;

  calculatorValue!: CalculatorComponentValue;

  @ViewChild(FormControlDirective, {static: true}) formControlDirective!: FormControlDirective;
  @Input() formControl!: FormControl;
  @Input() formControlName!: string;

  @ViewChild('equalTemplate', { static: false }) EQUAL_CARDS_TEMPLATE!: TemplateRef<any>;
  @ViewChild('notEqualTemplate', { static: false }) NOT_EQUAL_CARDS_TEMPLATE!: TemplateRef<any>;
  
  visibleCardsTemplate!: TemplateRef<any>;

  constructor(
    private calculatorService: CalculatorService,
    private controlContainer: ControlContainer
  ) { 
    this.subscription = new Subscription();
    this.form = new FormGroup({
      amount: new FormControl('', Validators.minLength(1))
    });
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.searchCombinationResult && !this.formControlDirective) {
        this.onSearchCombinationResult(this.searchCombinationResult);
      }
    })
  }

  onFormSubmit(): void {
    this.searchCombination(this.amount);
  }

  searchCombination(amount: number): void {
    this.subscription.add(this.calculatorService.searchCombination(amount, this.shopId)
    .subscribe(r => this.onSearchCombinationResult(r)))
  }

  setAmount(newAmount: CalculatorComponentValue): void {
    if (!newAmount)
      return;
    this.onChangeAmount.emit(newAmount.value);
    this.amount = newAmount.value;
    this.visibleCardsTemplate = this.EQUAL_CARDS_TEMPLATE;
  }

  onSearchCombinationResult(r: SearchCombinationResult): void {
    this.searchCombinationResult = r;
    if (r.equal) {
      this.setAmount(r.equal);
    } else {
      if (r.ceil && r.floor) {
        this.visibleCardsTemplate = this.NOT_EQUAL_CARDS_TEMPLATE;
      } else {
        const newAmount = r.ceil ? r.ceil : r.floor;
        this.chooseNewAmount(newAmount);
      }
    }
  }

  chooseNewAmount(newAmount: CalculatorComponentValue): void {
    this.searchCombinationResult = {
      equal: newAmount
    } as SearchCombinationResult;
    this.setAmount(newAmount);
  }

  onNextAmount(): void {
    this.searchCombination(this.amount ? this.amount+=1 : 1);
  }

  onPreviousAmount(): void {
    this.searchCombination(this.amount ? this.amount-=1 : 0);
  }

  writeValue(obj: any): void {
    this.onSearchCombinationResult({
      equal: obj
    } as SearchCombinationResult);
    this.formControlDirective?.valueAccessor?.writeValue(obj);
  }
  registerOnChange(onChange: any): void {
    this.formControlDirective?.valueAccessor?.registerOnChange(onChange);
  }
  registerOnTouched(onTouched: any): void {
    this.formControlDirective?.valueAccessor?.registerOnTouched(onTouched);
  }

  get amount(): number {
    return this.form.get('amount')?.value;
  }

  set amount(value: number) {
    this.form.get('amount')?.setValue(value);
  }

  get control(): FormControl {
    return this.formControl || this.controlContainer?.control?.get(this.formControlName);
  }
}