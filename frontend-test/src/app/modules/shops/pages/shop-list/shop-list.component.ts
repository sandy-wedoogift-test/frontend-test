import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Shop } from '../../models/shop.interface';
import { ShopService } from '../../services/shop/shop.service';

@Component({
  selector: 'app-shop-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.scss']
})
export class ShopListComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription;

  shops!: Array<Shop>;

  constructor(
    private shopService: ShopService
  ) { 
    this.subscriptions = new Subscription();

    this.subscriptions.add(this.shopService.getShopList().subscribe(r => this.shops = r.shopList));
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.subscriptions)
      this.subscriptions.unsubscribe();
  }
}
