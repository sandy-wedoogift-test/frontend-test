import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchCombinationComponent } from './search-combination.component';

describe('SearchCombinationComponent', () => {
  let component: SearchCombinationComponent;
  let fixture: ComponentFixture<SearchCombinationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchCombinationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchCombinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
