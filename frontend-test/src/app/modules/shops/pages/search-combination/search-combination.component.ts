import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-combination',
  templateUrl: './search-combination.component.html',
  styleUrls: ['./search-combination.component.scss']
})
export class SearchCombinationComponent implements OnInit {
  shopId: number;

  form: FormGroup;

  constructor(private activatedRoute: ActivatedRoute) { 
    const shopId = this.activatedRoute.snapshot.paramMap.get('shopId');
    this.shopId = shopId ? parseInt(shopId) : 0;

    this.form = this.form = new FormGroup({
      amountValue: new FormControl({
        cards: [20], 
        value: 20
      })
    });

    this.form.controls['amountValue']?.valueChanges.subscribe(v => {
      console.log('onChangeFormControl: ')
      console.log(v);
    });
  }

  ngOnInit(): void {
  }

  onChangeAmount(newAmount: number): void {
    console.log('onChangeAmount: ' + newAmount);
  }
}
