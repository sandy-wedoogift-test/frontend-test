import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { SearchCombinationComponent } from './pages/search-combination/search-combination.component';
import { ShopListComponent } from './pages/shop-list/shop-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'shop-list' },
  {
    path: '', 
    component: LandingPageComponent,
    children: [
      {path: 'shop-list', component: ShopListComponent},
      {path: ':shopId/search-combination', component: SearchCombinationComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopsRoutingModule { }
