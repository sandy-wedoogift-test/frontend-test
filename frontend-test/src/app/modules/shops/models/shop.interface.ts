export interface Shop {
    id: number;
    imgId: string;
    name: string;
}